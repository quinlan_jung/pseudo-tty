/*
 * pty.c
 *
 *  Created on: Dec 28, 2012
 *      Author: teamawesome
 */
#define BUFFERSIZE 150
#include "communicator.h"

int fd_master;
int fd_slave;
int valid;
extern char * buf;
extern int MAXDATASIZE;



void setup_pty(){ // open up the master and slave devices
	initialize_arrays();
	fd_master = posix_openpt(O_RDWR);

	if (fd_master < 0){
		printf("Error: openpty\n");
		exit(1);
	}

	valid = grantpt(fd_master);
	if (valid != 0){

		printf ("Error: grantpt %d\n", fd_master);
		exit(1);
	}

	valid = unlockpt(fd_master);
	if (valid !=0){
		printf ("Error: unlockpt\n");
		exit(1);
	}

	fd_slave = open (ptsname(fd_master), O_RDWR);
}

void parent_master (int socket){
	close (fd_slave);

	signal (SIGPIPE, SIG_IGN);
	fd_set fd_in;
	int nfds = 0;

	for (;;){
		FD_ZERO(&fd_in);
		FD_SET (socket, &fd_in);
		FD_SET (fd_master, &fd_in);

		nfds = max (nfds, socket);
		nfds = max (nfds, fd_master);
		valid = select (nfds+1, &fd_in, NULL, NULL, NULL);

		if (valid == -1){
			printf ("Error: select \n");
			exit(1);
		}
		else if (FD_ISSET(socket, &fd_in)){ // the terminal has sent us something -> forward it to user
			receive_data(socket);
			char * body = get_body (buf);
			strcat(body, "\n");
			write (fd_master, body, strlen(body));
		}

		else if (FD_ISSET(fd_master, &fd_in)){ //user has sent something ->forward it to terminal

			valid = read(fd_master, buf, MAXDATASIZE-1);
			buf[valid]='\0';

			if (valid==-1){
				printf ("Error: read \n");
				exit (1);
			}

			char * encapsulated_msg;
			long unsigned int string_length = strlen(buf);
			encapsulated_msg = encapsulate (buf, string_length, 'T');

			if (send(socket, encapsulated_msg, HEADER_SIZE+string_length, 0) == -1)
				perror("send");

			free (encapsulated_msg);
		}
	}
}

void child_slave (){
	struct termios slave_orig_term_settings; // Saved terminal settings
	struct termios new_term_settings; // Current terminal settings

	// CHILD

	// Close the master side of the PTY
	close(fd_master);

	// Save the defaults parameters of the slave side of the PTY
	valid = tcgetattr(fd_slave, &slave_orig_term_settings);

	// Set RAW mode on slave side of PTY
	new_term_settings = slave_orig_term_settings;
	cfmakeraw (&new_term_settings);
	tcsetattr (fd_slave, TCSANOW, &new_term_settings);

	// The slave side of the PTY becomes the standard input and outputs of the child process
	close(0); // Close standard input (current terminal)
	close(1); // Close standard output (current terminal)
	close(2); // Close standard error (current terminal)

	dup(fd_slave); // PTY becomes standard input (0)
	dup(fd_slave); // PTY becomes standard output (1)
	dup(fd_slave); // PTY becomes standard error (2)

	// Now the original file descriptor is useless
	close(fd_slave);

	// Make the current process a new session leader
	setsid();

	// As the child is a session leader, set the controlling terminal to be the slave side of the PTY
	// (Mandatory for programs like the shell to make them manage correctly their outputs)
	ioctl(0, TIOCSCTTY, 1);

	execvp("/bin/sh", NULL);
}

void communicate_pty ( int socket){

setup_pty();

if (fork()){
	parent_master(socket);
}

else{
	child_slave();
}

free_arrays();

}









