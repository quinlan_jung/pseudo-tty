/*
 * main.c
 *
 *  Created on: Nov 1, 2012
 *      Author: teamawesome
 */

#include "communicator.h"

char * 	host;
char * 	port;
int 	type; // connector = 0, acceptor = 1, echo = 2
int 	communication_socket;

int sockfd;  // listen on sock_fd, new connection on new_fd
int new_fd;

struct sockaddr_storage their_addr; // connector's address information
char s[INET6_ADDRSTRLEN];

int main(int argc, char * argv[]) {

	proper_usage(argc, argv);
	parse_args (argv, &host, &port, &type, &sockfd, &new_fd);

	// Set up all the socket options
	setup_socket(&type, &sockfd, s, host, port);

	// If its ACCEPTOR or ECHO, we need to listen and accept before communicating
	if (type == ACCEPTOR || type == ECHO_MODE || type == PTY){
		// Listen on sockfd
		if (listen(sockfd, BACKLOG) == -1) {perror("listen"); exit(1);}

		// Wait for a new connection on sockfd
		printf("server: waiting for connections...\n");
		socklen_t sin_size = sizeof (struct sockaddr_storage);
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) {perror("accept");exit(1);}

		// Look up the other end's address
		lookup_connection(their_addr.ss_family,
						  get_in_addr((struct sockaddr *)&their_addr),
						  s,
						  sizeof s);

		// Ensure we communicate over the new fd, not the original one
		communication_socket = new_fd;

		// We don't need the original socket any more, so close it
		close(sockfd);
	}

	else{
		// Ensure we communicate over the original fd
		communication_socket = sockfd;
	}

	// Now everything is set up. Call subroutine to communicate with other end
	if (type == PTY){
		communicate_pty (communication_socket);
	}
	else{
		communicate(communication_socket, &type);
	}

	exit(0);

}
