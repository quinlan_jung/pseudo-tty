/*
 * communicator_simp2.c
 *
 *  Created on: Sep 30, 2012
 *      Author: teamawesome
 */

#include "communicator.h"

struct addrinfo hints, *servinfo, *p;
int rv;
int MAXDATASIZE = DEFAULT_SIZE;

//acceptor specific
int yes = 1;
struct sigaction sa;

//connector specific
char * buf;

void initialize_arrays(){
	buf = calloc (MAXDATASIZE, 1);
}

void free_arrays(){
	free (buf);
}

void realloc_buffer_prn (int numbytes){
	if (numbytes>=MAXDATASIZE){
			MAXDATASIZE = 2*MAXDATASIZE;
			buf = realloc(buf, MAXDATASIZE);
			memset(buf+(MAXDATASIZE/2), 0, (MAXDATASIZE/2));

		}
}

void sigchld_handler(int s) {
	while (waitpid(-1, NULL, WNOHANG) > 0)
		;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*) sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*) sa)->sin6_addr);
}

void lookup_connection(int family, void * address, char * buffer, int size) {
	inet_ntop(family, address, buffer, size);
	printf("got connection from %s\n", buffer);
}

int valid_port (char * port){
	int result;
	if ((result =atoi(port))){
		if (result > 0){
			return result;
		}
		else{
			return 0;
		}
	}
	return 0;
}

int valid_ipv4_block(char* block){

	char * endptr;
	int result = strtol(block, &endptr, 10);
	if (endptr == block){ //A non-integer was passed
		return 0;
	}
	else if (result>255 || result<0){//out of range value was passed
		return 0;
	}

	return 1;
}

int valid_ipv4 (char * ip){
	char ip_temp [20];
	strcpy(ip_temp, ip);
	char * block;
	int block_record = 1;
	int iterations = 0;

	block = strtok(ip_temp, ".");
	while (block != NULL){
		block_record = block_record & valid_ipv4_block(block);
		block = strtok(NULL, ".");
		iterations++;
	}
	return block_record && iterations==4;
}

void proper_usage (int argc, char * argv[]){

	if (argc == 4 && !strcmp(argv[1], "connector")) {
		if (!valid_ipv4(argv[2]) || !valid_port(argv[3])){
			fprintf(stderr,"usage: connector hostname port\n");
			exit(1);
		}
	}

	else if (argc == 3 && !strcmp(argv[1], "acceptor")) {
		if (!valid_port (argv[2])){ // invalid port
			fprintf(stderr,"usage: acceptor port\n");
			exit(1);
		}
	}
	else if (argc == 3 && !strcmp(argv[1], "echo")){
		if (!valid_port (argv[2])){ //invalid port
			fprintf(stderr,"usage: echo port\n");
			exit (1);
		}
	}

	else if (argc == 3 && !strcmp(argv[1], "pty")){
		if (!valid_port (argv[2])){
			fprintf (stderr, "usage: pty port \n");
			exit(1);
		}
	}

	else { // type isnt anything
		fprintf (stderr, "this isnt a proper command");
		fprintf(stderr,"usage: {connector, acceptor, echo, pty} {hostname} {port}\n");
		exit(1);
	}

}

void parse_args (char * argv[], char **host, char ** port, int * type, int * sockfd, int * new_fd){

	if (!strcmp(argv[1], "connector")) {
		printf("THIS IS THE CONNECTOR \n"); // everything is done through *sockfd
		*type = CONNECTOR;
		*host = argv[2];
		*port = argv[3];
	}

	else if (!strcmp(argv[1], "acceptor")) {
		printf("THIS IS THE ACCEPTOR \n"); //everything is done through new_fd
		*type = ACCEPTOR;
		*port = argv[2];
	}

	else if (!strcmp(argv[1], "echo")) {
		printf("this is the ECHO \n");
		*type = ECHO_MODE;
		*port = argv[2];
	}

	else if (!strcmp(argv[1], "pty")) {
			printf("THIS IS THE PTY \n"); //everything is done through new_fd
			*type = PTY;
			*port = argv[2];
		}

}


void set_hints (int * type){
	memset (&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if (*type == ACCEPTOR || *type == ECHO_MODE || *type == PTY){
		hints.ai_flags = AI_PASSIVE;
	}
}


void get_server_info (char * host, char * port){
	if ((rv = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
				fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
				exit(1);
			}
}

void setup_socket(int * type, int * sockfd, char s [INET6_ADDRSTRLEN], char *host, char *port){

	set_hints(type);
	get_server_info(host, port);
	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((*sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("socket problem");
			continue;
		}

		if (*type == CONNECTOR){
			if (connect(*sockfd, p->ai_addr, p->ai_addrlen) == -1) {
				close(*sockfd);
				perror("client: connect");
				continue;
			}

			lookup_connection(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),s, sizeof s);
		}

		if (*type == ACCEPTOR || *type == ECHO_MODE || *type == PTY){
			if (setsockopt(*sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
					sizeof(int)) == -1) {
				perror("setsockopt");
				exit(1);
			}

			if (bind(*sockfd, p->ai_addr, p->ai_addrlen) == -1) {
				close(*sockfd);
				perror("server: bind");
				continue;
			}
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "failed to setup socket\n");
		exit(1);
	}

	freeaddrinfo(servinfo); // all done with this structure

}


void receive_data (int socket){
	int body_length = 0;
	int numbytes = 0;
	numbytes = numbytes + recv(socket, buf+numbytes, MAXDATASIZE-numbytes, 0);
	realloc_buffer_prn(numbytes);

	//grab the entire header first
	while (numbytes < HEADER_SIZE){
		numbytes = numbytes + recv(socket, buf+numbytes, MAXDATASIZE-numbytes, 0);
		realloc_buffer_prn(numbytes);
	}

	// end of function, exit program
	if (get_type(buf) == 'E'){
		free_arrays();
		close(socket);
		exit(1);
	}

	body_length =  process_little_endian (buf); //Calculate body length

	while (numbytes < body_length + HEADER_SIZE){
		numbytes = numbytes + recv(socket, buf+numbytes, MAXDATASIZE-numbytes, 0);
		realloc_buffer_prn(numbytes);
	}

	buf[numbytes]= '\0';

	if (numbytes == -1) {
		perror("recv");
		exit(1);
	}

	if (!numbytes) {
		printf("%s", "EOF Detected. Program exiting.\n");
		exit(0);
	}
}


void communicate(int socket, int * type){
	initialize_arrays();


	signal (SIGPIPE, SIG_IGN);
	int nfds = 0;
	fd_set rd;

	for (;;){
		FD_ZERO (&rd);
		FD_SET (socket, &rd); //add socket to rd

		if (*type == ACCEPTOR || *type == CONNECTOR || *type == PTY){
			FD_SET (0, &rd); // add stdin to rd
			printf("%s", "Enter a line of text to send: \n");
		}


		nfds = max (nfds, socket);
		select (nfds+1, &rd, NULL, NULL, NULL);

		if (FD_ISSET (0, &rd) && (*type == ACCEPTOR || *type == CONNECTOR || *type == PTY)){
			char * encapsulated_msg;

			if (!fgets(buf, MAXDATASIZE-HEADER_SIZE, stdin)) { // limit command to MAXDATASIZE-HEADERSIZE
				buf[0] = '\0';
				encapsulated_msg = encapsulate (buf, 1, 'E');
				if (send(socket, encapsulated_msg, HEADER_SIZE+1, 0) == -1)
					perror("send");
				printf ("\n%s", "EOF Detected. Program ending.\n");
				free (encapsulated_msg);
				exit (0);
			}

			long unsigned int string_length = strlen (buf);
			buf[string_length-1]='\0';
			encapsulated_msg = encapsulate (buf, string_length, 'T');
			if (send(socket, encapsulated_msg, HEADER_SIZE+string_length, 0) == -1)
				perror("send");
			free (encapsulated_msg);

		}

		if (FD_ISSET (socket, &rd)){
			receive_data(socket);

			CURRENT_MODE == REGULAR_MODE? printf("%s\n", get_body(buf)): decapsulate (buf);

			if (*type == ECHO_MODE){
				if (send(socket, buf, HEADER_SIZE+ strlen(HEADER_SIZE+ buf)+1, 0) == -1)
								perror("send");
			}
		}
	}

	free_arrays();
	close(socket);
}












