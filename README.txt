=================    
    OVERVIEW
=================
This program lets you access a terminal on another machine.

=================
    HOW TO USE
==================

Instructions for program:

1. Navigate to the directory where the files are, and type "make"
2. On the acceptor/server side, type "./communicator pty <port#>"
3. On the connector side, type "./communicator connector <ipv4 addr> <port#>
