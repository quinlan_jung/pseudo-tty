CFLAGS = -Wall -Werror -std=c99 -g

all: communicator_simp2.c main.c protocol.c pty.c communicator.h
	gcc ${CFLAGS} -o communicator communicator_simp2.c protocol.c pty.c main.c 
	
clean:
	-rm -rf communicator


