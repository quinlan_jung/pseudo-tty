/*
 * communicator.h
 *
 *  Created on: Nov 1, 2012
 *      Author: teamawesome
 */

#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

//===========MODE==============================
#define CURRENT_MODE REGULAR_MODE

#define REGULAR_MODE 0
#define DEBUG_MODE 1

//=========== APPLICATION PROTOCOL ===========
#define NUMBER_SIZE 5 //# of bytes representing msg length
#define TYPE_SIZE 1 //# of bytes representing msg type
#define HEADER_SIZE NUMBER_SIZE+TYPE_SIZE

char * encapsulate (char * msg, long unsigned int str_length, char msg_type);
void decapsulate (char * encap_msg);
long unsigned int process_little_endian (char * array);
char * get_body (char * encap_msg);
char get_type (char * encap_msg);
//===============================================

//=========== CONNECTOR/ACCEPTOR/ECHO CONNECTION ===========
#define DEFAULT_SIZE 1024
#define BACKLOG 10     // how many pending connections queue will hold
#define CONNECTOR 0
#define ACCEPTOR 1
#define ECHO_MODE 2
#define PTY 3

#undef max
#define max(x,y) ((x) > (y) ? (x) : (y))

void proper_usage (int argc, char * argv[]);
void parse_args (char * argv[], char * *host, char * *port, int * type, int * sockfd, int * new_fd);
void set_hints (int * type);
void get_server_info (char * host, char * port);
void setup_socket(int * type, int * sockfd, char s [INET6_ADDRSTRLEN], char* host, char* port);
void lookup_connection(int family, void * address, char * buffer, int size);
void communicate(int socket, int * type);
void *get_in_addr(struct sockaddr *sa);
void initialize_arrays(void);
void free_arrays(void);
void receive_data (int socket);
void communicate_pty ( int socket);

//===============================================

#endif /* COMMUNICATOR_H_ */
