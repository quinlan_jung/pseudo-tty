/*
 * protocol.c
 *
 *  Created on: Nov 23, 2012
 *      Author: teamawesome
 */

#include "communicator.h"

void convert_little_endian (char * array, long unsigned int str_length){
	long unsigned int temp = str_length;
	char temp_byte;
	long unsigned int bit_mask;
	for (int i=0; i<NUMBER_SIZE;i++){
		bit_mask = 0xFF << (8*i);
		temp = (str_length & bit_mask);
		temp = temp >> (8*i);
		temp_byte = temp;
		array[TYPE_SIZE+i] = temp_byte;
	}
}

long unsigned int process_little_endian (char * array){
	long unsigned int result = 0;
	long unsigned int temp_number;
	for (int i=0; i< NUMBER_SIZE; i++){
		temp_number = (uint8_t) array[TYPE_SIZE+i] << (8*i);
		result = result + temp_number;
	}
	return result;
}



char * encapsulate (char * msg, long unsigned int str_length, char msg_type){
	char * encapsulate_msg = calloc (str_length+TYPE_SIZE+NUMBER_SIZE+1, sizeof (char));
	encapsulate_msg[0] = msg_type;
	convert_little_endian(encapsulate_msg, str_length);
	strcat(encapsulate_msg+TYPE_SIZE+NUMBER_SIZE, msg);

	return encapsulate_msg;
}

void decapsulate (char * encap_msg){
	printf ("\n");
	for (int i = 0 ; i< TYPE_SIZE; i++){
		printf("Type: %c\n", encap_msg[i]);
	}
	printf ("Length: %ldu \n", process_little_endian(encap_msg));
	printf ("Message: %s \n", encap_msg+NUMBER_SIZE+TYPE_SIZE);
	printf ("\n");
}

// for the one letter protocol
char get_type (char * encap_msg){
	return encap_msg[0];
}


char * get_body (char * encap_msg){
	return encap_msg + NUMBER_SIZE+ TYPE_SIZE ;
}

